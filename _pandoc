#compdef pandoc
# Written for pandoc 1.19.2.1

_pandoc() {
  local context curcontext="$curcontext" state line cmds update_policy ret=1

  [[ ${+commands[${words[1]}]} -eq 0 ]] && return 0

  _arguments -S -s \
    '(--from -f --read -r)'{--from=,-f+}'[Specify input format]:fromformat:->fromformat' \
    '(--to -t --write -w)'{--to=,-t+}'[Specify output format]:toformat:->toformat' \
    '(--output -o)'{--output,-o}'[Output filename]' \
    '--data-dir=[Directory to search for pandoc data files]:directory:_files' \
    '(--parse-raw -R)'{--parse-raw,-R}'[Parse untranslatable HTML codes and LaTeX environments as raw HTML or LaTeX]' \
    '(--smart -S)'{--smart,-S}'[Produce typographically correct output]' \
    '--old-dashes[Selects the pandoc <= 1.8.2.1 behavior for parsing smart dashes]' \
    '--base-header-level=[Specify the base level for headers (defaults to 1)]:number' \
    '--indented-code-classes=[Specify classes to use for indented code blocks]' \
    '(--filter -F)'{--filter=,-F+}'[Specify an executable to be used as a filter on the Pandoc AST]' \
    '--normalize[Normalize the document after reading]' \
    '(--preserve-tabs -p)'{--preserve-tabs,-p}'[Preserve tabs instead of converting them to spaces (the default)]' \
    '--tab-stop=[Specify the number of spaces per tab (default: 4)]:number' \
    '--track-changes=[accept|reject|all]' \
    '--file-scope[Parse each file individually before combining for multifile documents]' \
    '--extract-media=[Extract images and other media contained in a docx or epub container to the path DIR]:directory:_files' \
    '(--standalone -s)'{--standalone,-s}'[Produce output with an appropriate header and footer]' \
    '--template=[Use file for a custom template for the generated document]:file:_files' \
    '(--metadata -M)'{--metadata=,-M+}'[Set the metadata field KEY to the value VAL]' \
    '(--variable -V)'{--variable=,-V+}'[Set the template variable KEY to the value VAL when rendering the document in standalone mode]' \
    '-D[Print the system default template for an output FORMAT]' \
    '--print-default-data-file=[Print a system default data file]:file:_file_' \
    '--dpi=[Specify the dpi value for conversion from pixels to inch/centimeters and vice versa]:number' \
    '--wrap=[Determine how text is wrapped in the output]:wrap:->wrap' \
    '--columns=[Specify length of lines in characters]:number' \
    '(--table-of-contents --toc)'{--table-of-contents,--toc}'[Include an automatically generated table of contents]' \
    '--toc-depth=[Specify the number of section levels to include in the table of contents (default: 3)]:number' \
    '--no-highlight[Disables syntax highlighting]' \
    '--highlight-style=[Specifies the coloring style to be used in highlighted source code]' \
    '(--include-in-header -H)'{--include-in-header=,-H+}'[Include contents of FILE, verbatim, at the end of the header]:file:_files' \
    '(--include-before-body -B)'{--include-before-body=,-B+}'[Include contents of FILE, verbatim, at the beginning of the document body]:file:_files' \
    '(--include-after-body -A)'{--include-after-body=,-A+}'[Include contents of FILE, verbatim, at the end of the document body]:file:_files' \
    '--self-contained[Produce a standalone HTML file using data URIs]' \
    '--html-q-tags[Use <q> tags for quotes in HTML]' \
    '--ascii[Use only ASCII characters in output]' \
    '--reference-links[Use reference-style links rather than inline-style links in Markdown or reStructuredText]' \
    '--reference-location=[Specify where footnotes are placed (default: document)]:refloc:->refloc' \
    '--atx-headers[Use ATX-style headers in Markdown and AsciiDoc output]' \
    '--top-level-division=[Treat top-level headers as the given division type in LaTeX, ConTeXt, DocBook, and TEI output]:topleveldiv:->topleveldiv' \
    '(--number-sections -N)'{--number-sections,-N}'[Number section headings in LaTeX, ConTeXt, HTML, or EPUB output]' \
    '--number-offset=[Offset for section headings in HTML output]:number' \
    '--no-tex-ligatures[Do not use the TeX ligatures for quotation marks, apostrophes, and dashes when writing or reading LaTeX or ConTeXt]' \
    '--listings[Use the listings package for LaTeX code blocks]' \
    '(--incremental -i)'{--incremental,-i}'[Make list items in slide shows display incrementally (one by one)]' \
    '--slide-level=[Specifies that headers with the specified level create slides]:number' \
    '--section-divs[Wrap sections in <div> tags (or <section> tags in HTML5)]' \
    '--default-image-extension=[Specify a default extension to use when image paths/URLs have no extension]' \
    '--email-obfuscation=[Specify a method for obfuscating mailto: links in HTML documents]:emailobf:->emailobf' \
    '--id-prefix=[Specify a prefix to be added to all automatically generated identifiers/footnotes (HTML, DocBook, Markdown)]' \
    '-T=[Specify a prefix at the beginning of the title that appears in the HTML header]' \
    '(--css -c)'{--css=,-c+}'[Link to a CSS style sheet]' \
    '--reference-odt=[Use the specified file as a style reference in producing an ODT]:file:_files' \
    '--reference-docx=[Use the specified file as a style reference in producing a docx file]:file:_files' \
    '--epub-stylesheet=[Use the specified CSS file to style the EPUB]:file:_files' \
    '--epub-cover-image=[Use the specified image as the EPUB cover]:file:_files' \
    '--epub-metadata=[Look in the specified XML file for metadata for the EPUB]:file:_files' \
    '--epub-embed-font=[Embed the specified font in the EPUB]:file:_files' \
    '--epub-chapter-level=[Specify the header level at which to split the EPUB into separate "chapter" files]:number' \
    '--latex-engine=[Use the specified LaTeX engine when producing PDF output]:latexengine:->latexengine' \
    '--latex-engine-opt=[Use the given string as a command-line argument to the latex-engine]' \
    '--bibliography=[Set the bibliography field to a file, and process citations using pandoc-citeproc]:file:_files' \
    '--csl=[Set the csl (citation style language) field to a file]:file:_files' \
    '--citation-abbreviations=[Set the citation-abbreviations field to a file]:file:_files' \
    '--natbib[Use natbib for citations in LaTeX output]' \
    '--biblatex[Use biblatex for citations in LaTeX output]' \
    '(--latexmathml --asciimathml -m)'{--latexmathml=,--asciimathml=,-m+}'[Use the LaTeXMathML script to display embedded TeX math in HTML output]' \
    '--mathml=[Convert TeX math to MathML]' \
    '--mimetex=[Render TeX math using the mimeTeX CGI script]' \
    '--webtex=[Render TeX formulas using an external script that converts TeX formulas to images]' \
    '--jsmath=[Use jsMath to display embedded TeX math in HTML output]' \
    '--mathjax=[Use MathJax to display embedded TeX math in HTML output]' \
    '--katex=[Use KaTeX to display embedded TeX math in HTML output]' \
    '--katex-stylesheet=[The URL should point to the katex.css stylesheet]' \
    '--gladtex[Enclose TeX math in <eq> tags in HTML output]' \
    '--trace' \
    '--dump-args[Print information about command-line arguments to stdout, then exit]' \
    '--ignore-args[Ignore command-line arguments]' \
    '--verbose[Give verbose debugging output]' \
    '--bash-completion[Generate a bash completion script]' \
    '--list-input-formats[List supported input formats]' \
    '--list-output-formats[List supported output formats]' \
    '--list-extensions[List supported Markdown extensions]' \
    '--list-highlight-languages[List supported languages for syntax highlighting]' \
    '--list-highlight-styles[List supported styles for syntax highlighting]' \
    '(--version -v)'{--version,-v}'[Print version]' \
    '(--help -h)'{--help,-h}'[Show usage message]' \
    '*:file:_files' \
    && ret=0

  case $state in
    fromformat)
      local fromformats; fromformats=(
        'commonmark'
        'docbook'
        'docx'
        'epub'
        'haddock'
        'html'
        'json'
        'latex'
        'markdown'
        'markdown_github'
        'markdown_mmd'
        'markdown_phpextra'
        'markdown_strict'
        'mediawiki'
        'native'
        'odt'
        'opml'
        'org'
        'rst'
        't2t'
        'textile'
        'twiki'
      )
      _describe -t 'fromformats' 'fromformat' fromformats && ret=0
      ;;
    toformat)
      local toformats; toformats=(
        'asciidoc'
        'beamer'
        'commonmark'
        'context'
        'docbook'
        'docbook5'
        'docx'
        'dokuwiki'
        'dzslides'
        'epub'
        'epub3'
        'fb2'
        'haddock'
        'html'
        'html5'
        'icml'
        'json'
        'latex'
        'man'
        'markdown'
        'markdown_github'
        'markdown_mmd'
        'markdown_phpextra'
        'markdown_strict'
        'mediawiki'
        'native'
        'odt'
        'opendocument'
        'opml'
        'org'
        'plain'
        'revealjs'
        'rst'
        'rtf'
        's5'
        'slideous'
        'slidy'
        'tei'
        'texinfo'
        'textile'
        'zimwiki'
      )
      _describe -t 'toformats' 'toformat' toformats && ret=0
      ;;
    wrap)
      local wrap; wrap=(
        'auto'
        'none'
        'preserve'
      )
      _describe -t 'wrap' 'wrap' wrap && ret=0
      ;;
    refloc)
      local refloc; refloc=('block' 'document' 'section')
      _describe -t 'refloc' 'refloc' refloc && ret=0
      ;;
    topleveldiv)
      local topleveldiv; topleveldiv=('section' 'chapter' 'part')
      _describe -t 'topleveldiv' 'topleveldiv' topleveldiv && ret=0
      ;;
    emailobf)
      local emailobf; emailobf=('none' 'javascript' 'reference')
      _describe -t 'emailobf' emailobf emailobf && ret=0
      ;;
    latexengine)
      local latexengine; latexengine=('pdflatex' 'lualatex' 'xelatex')
      _describe -t 'latexengine' latexengine latexengine && ret=0
      ;;
  esac

  return ret
}
